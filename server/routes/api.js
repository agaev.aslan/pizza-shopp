const express = require('express');
const api = require('../controllers/api')
const router = express.Router();

router.get('/get-menu', api.getMenu)
router.post('/create-order', api.createOrder)

module.exports = router;