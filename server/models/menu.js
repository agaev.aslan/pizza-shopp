module.exports = (sequelize, DataTypes) => {
    const Menu = sequelize.define('menu', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        name: {
            type: DataTypes.CHAR,
            allowNull: false,
        },
        category: {
            type: DataTypes.CHAR,
        },
        price: {
            type: DataTypes.DECIMAL,
            allowNull: false,
        },
        description: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        image_url: {
            type: DataTypes.TEXT,
        },
    });
    return Menu;
};
