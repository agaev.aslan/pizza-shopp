module.exports = (sequelize, DataTypes) => {
    const Order = sequelize.define('orders', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        firstname: {
            type: DataTypes.CHAR,
            allowNull: false,
        },
        surname: {
            type: DataTypes.CHAR,
            allowNull: false,
        }, 
        amount: {
            type: DataTypes.DECIMAL,
            allowNull: false,
        }, 
        phone: {
            type: DataTypes.CHAR,
            allowNull: false,
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        account_id: {
            type: DataTypes.INTEGER,
        }
    });
    return Order;
};
