const Sequelize = require('sequelize');

const connectionString = process.env.CLEARDB_DATABASE_URL;

const sequelize = new Sequelize(connectionString, {
    logging: false,
    dialect: 'mysql',
    protocol: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 20000,
        acquire: 200000,
    },
});

sequelize.sync().then(() => {
    console.log('Database Synced');
});

const Menu = require('./menu')(sequelize, Sequelize)
const Order = require('./orders')(sequelize, Sequelize)

models = {
    Menu,
    Order
}

models.sequelize = sequelize;
models.Sequelize = Sequelize;

module.exports = models;

