require('dotenv').config()
const express = require('express');
const path = require('path');
const cors = require('cors');

const apiRoutes = require('./routes/api');

const app = express();

const corsOptions = {
    origin: (origin, callback) => {
        return callback(null, true);
    },
    credentials: true,
};
app.options('*', cors(corsOptions));
app.use(cors(corsOptions));

app.use(apiRoutes);
app.use(express.static(path.join(__dirname, '/../client/build')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/../client/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`Express server app on ${port}`);


