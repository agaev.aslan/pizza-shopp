const models = require('../models');
const nodemailer = require("nodemailer");
const helper = require('sendgrid').mail;
const sg = require('sendgrid')(process.env.SENDGRID_API_KEY);

const getMenu = async (req, res) => {
    try {
        const menu = await models.Menu.findAll()
        res.send(menu)
    } catch (error) {
        res.status(500).send(error)
    }
};

const createOrder = async (req, res) => {
    try {
        const data = req.query
        const order = models.Order.create(data)
        sendConfirmationLetterSg(data)
        res.send(order)
    } catch (error) {
        res.status(500).send(error)
    }
};

const sendConfirmationLetterSg = async (data) => {
    const from_email = new helper.Email(process.env.MAIL_USER);
    const to_email = new helper.Email(data.email);
    const subject = 'Confirmation, pizza test app'
    const msgText = `Hello! Your order has been received.\n
Order info:\nName: ${data.firstname} ${data.surname}\nPhone: ${data.phone}\nPrice: ${data.amount}$`
    const content = new helper.Content('text/plain', msgText);
    const mail = new helper.Mail(from_email, subject, to_email, content);

    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });

    sg.API(request, function (error, response) {
        console.log(response.statusCode);
        console.log(response.body);
        console.log(response.headers);
    });
}

const sendConfirmationLetter = async (data) => {
    try {
        let transporter = nodemailer.createTransport({
            host: "smtp.yandex.ru",
            port: 465,
            secure: true,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASS,
            }
        });

        const msgText = `Hello! Your order has been received.\n
    Order info:\nName: ${data.firstname} ${data.surname}\nPhone: ${data.phone}\nPrice: ${data.amount}$`

        let info = await transporter.sendMail({
            from: process.env.MAIL_USER,
            to: data.email,
            subject: "Confirmation, pizza test app",
            text: msgText,
        });
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    getMenu,
    createOrder,
};
