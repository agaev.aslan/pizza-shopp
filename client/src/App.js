import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.scss';
import { actions } from './_actions'


import NavigationBar from './navbar/NavigationBar'
import Body from './Body/Body'

class App extends Component {
  componentDidMount() {
    this.props.getMenu()
    if (typeof this.props.currencyMode == 'undefined') {
      this.props.setCurrency()
    }
    if (typeof this.props.euroRate == 'undefined')
      this.props.setEuroRate()
  }

  render() {
    return (
      <div className="App">
        <NavigationBar />
        <Body />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currencyMode: state.orders.currencyMode,
  loading:  state.orders.loading,
  euroRate: state.orders.euroRate,
})

const mapActionsToProps = {
  setCurrency: actions.setCurrency,
  setEuroRate: actions.setEuroRate,
  getMenu: actions.getMenu,
}

export default connect(mapStateToProps, mapActionsToProps)(App);
