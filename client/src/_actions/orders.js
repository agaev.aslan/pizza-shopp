import { api } from '../_services/api'

export const SET_CURRENCY_REQUEST = 'SET_CURRENCY_REQUEST'
export const SET_CURRENCY_SUCCESS = 'SET_CURRENCY_SUCCESS'
export const SET_EURO_RATE_REQUEST = 'SET_EURO_RATE_REQUEST'
export const SET_EURO_RATE_SUCCESS = 'SET_EURO_RATE_SUCCESS'
export const GET_MENU_REQUEST = 'GET_MENU_REQUEST'
export const GET_MENU_SUCCESS = 'GET_MENU_SUCCESS'
export const SET_TOTAL_PRICE = 'SET_TOTAL_PRICE'

function setCurrency(mode) {
    return dispatch => {
        dispatch(request())
        if (mode === '€')
            dispatch(success('€'))
        else {
            dispatch(success('$'))
        }
    }
    function request() { return { type: SET_CURRENCY_REQUEST } }
    function success(currencyMode) { return { type: SET_CURRENCY_SUCCESS, currencyMode } }
}

function getMenu() {
    return dispatch => {
        dispatch(request())
        api.getMenu().then(data => {
            const items = data.data.map(item => {
                item.selectedAmount = 0
                return item
            })
            dispatch(success(items))
            dispatch(setTotalPrice(0))
        })
    }
    function request() { return { type: GET_MENU_REQUEST } }
    function success(menuItems) { return { type: GET_MENU_SUCCESS, menuItems } }
    function setTotalPrice(price) { return { type: SET_TOTAL_PRICE, price  } }
}

function setEuroRate() {
    return dispatch => {
        dispatch(request())
        api.getEuroRate().then(rate => dispatch(success(rate)))
    }

    function request() { return { type: SET_EURO_RATE_REQUEST } }
    function success(euroRate) { return { type: SET_EURO_RATE_SUCCESS, euroRate } }
}

function selectItem(id, add, menuItems, totalPrice) {
    return dispatch => {
        let price = parseFloat(totalPrice) 
        const items = menuItems.map(item => {
            if (item.id === id) {
                if (add) {
                    item.selectedAmount += 1
                    price += parseFloat(item.price)
                }
                else if (item.selectedAmount > 0) {
                    item.selectedAmount -= 1
                    price -= item.price
                }
            }
            return item
        })
        dispatch(success(items))
        dispatch(setTotalPrice(parseFloat(price)))
    }
    function success(menuItems) { return { type: GET_MENU_SUCCESS, menuItems } }
    function setTotalPrice(price) { return { type: SET_TOTAL_PRICE, price } }
}

function clearOrderInfo(menuItems) {
    return dispatch => {
        const items = menuItems.map(item => {
            item.selectedAmount = 0
            return item
        })
        dispatch(success(items))
        dispatch(setTotalPrice(0))
    }
    function success(menuItems) { return { type: GET_MENU_SUCCESS, menuItems } }
    function setTotalPrice(price) { return { type: SET_TOTAL_PRICE, price } }
}

export const actions = {
    setCurrency,
    setEuroRate,
    getMenu,
    selectItem,
    clearOrderInfo
} 