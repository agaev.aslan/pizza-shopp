import React, { Component } from 'react'
import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { actions } from '../_actions'

class NavigationBar extends Component {
    render() {
        if (!this.props.loading) {
            let mode
            mode = this.props.currencyMode ? this.props.currencyMode : '$$'
            const total = this.props.totalPrice ? this.props.totalPrice : 0
            const totalPrice = this.props.currencyMode == '$' ? total : parseFloat(total*this.props.euroRate).toFixed(2)
            return (
                <div>
                    <Navbar collapseOnSelect expand="lg" bg="light" variant="light" sticky="top">
                        <Navbar.Brand href="/">Pizza shop</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link as={Link} to="/">Home page</Nav.Link>
                                <Nav.Link as={Link} to="menu">Menu</Nav.Link>
                            </Nav>
                            <Nav>
                                <Nav.Link as={Link} to="cart">Cart<b>({totalPrice})</b></Nav.Link>
                                <NavDropdown title={mode} id="collasible-nav-dropdown">
                                    <NavDropdown.Item onClick={() => this.props.setCurrency('$')}>$</NavDropdown.Item>
                                    <NavDropdown.Item onClick={() => this.props.setCurrency('€')}>€</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </div >
            )
        } else return null
    }
}

const mapStateToProps = state => ({
    currencyMode: state.orders.currencyMode,
    loading: state.orders.loading,
    euroRate: state.orders.euroRate,
    totalPrice: state.orders.totalPrice,
})

const mapActionsToProps = {
    setCurrency: actions.setCurrency,
    setEuroRate: actions.setEuroRate,
}

export default connect(mapStateToProps, mapActionsToProps)(NavigationBar);