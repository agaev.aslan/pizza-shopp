
import {
    SET_CURRENCY_REQUEST, SET_CURRENCY_SUCCESS, SET_EURO_RATE_REQUEST, SET_EURO_RATE_SUCCESS,
    GET_MENU_REQUEST, GET_MENU_SUCCESS, SET_TOTAL_PRICE, CLEAR_ORDER_INFO
} from '../_actions/index'

export function orders(state = {}, action) {
    switch (action.type) {
        case SET_CURRENCY_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case SET_CURRENCY_SUCCESS:
            return {
                ...state,
                loading: false,
                currencyMode: action.currencyMode,
            };
        case SET_EURO_RATE_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case SET_EURO_RATE_SUCCESS:
            return {
                ...state,
                loading: false,
                euroRate: action.euroRate,
            };
        case GET_MENU_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case GET_MENU_SUCCESS:
            return {
                ...state,
                loading: false,
                menuItems: action.menuItems,
            };
        case SET_TOTAL_PRICE:
            return {
                ...state,
                totalPrice: action.price,
            };
        default:
            return state
    }
}