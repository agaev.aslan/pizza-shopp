import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom';
import Menu from '../menu/Menu'
import Home from '../Home/Home'
import Cart from '../Cart/Cart'

class Body extends Component {
    render() {
        return (
            <div>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/menu' component={Menu} />
                    <Route path='/cart' component={Cart} />
                </Switch>
            </div>
        )
    }
}

export default Body;