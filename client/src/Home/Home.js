import React, { Component } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import './style.css'

class Home extends Component {
    render() {
        return (
            <div>
                <Container className="home-back">
                    <Row className="justify-content-md-center">
                        <Col>
                            <div className="text">
                                Hungry?
                                <br/>
                                Go to the <a className="home-link" href="/menu">menu</a> to find the best pizza! 
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}

export default Home;