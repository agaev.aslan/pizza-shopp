import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { actions } from '../_actions'
import Cart from '../Cart/Cart'

class OrderModal extends Component {
    render() {
        if (!this.props.loading) {
            return (
                <div>
                    <Modal
                        {...this.props}
                        size="lg"
                        aria-labelledby="contained-modal-title-vcenter"
                        centered
                    >
                        <Modal.Header closeButton>
                            <Modal.Title id="contained-modal-title-vcenter">
                                Your order
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Cart />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="outline-secondary" size="sm" style={{ margin: '3px' }} onClick={this.props.onHide}>Close</Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            )
        } else {
            return (<div>Loading...</div>)
        }
    }
}

const mapStateToProps = state => ({
    currencyMode: state.orders.currencyMode,
    loading: state.orders.loadin,
    euroRate: state.orders.euroRat,
})

const mapActionsToProps = {
    setCurrency: actions.setCurrency,
    setEuroRate: actions.setEuroRate,
}

export default connect(mapStateToProps, mapActionsToProps)(OrderModal);