import React, { Component } from 'react'
import { Container, Row, Card, ListGroup, ListGroupItem, Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { actions } from '../_actions'
import OrderModal from './OrderModal'

class Menu extends Component {
    constructor(props) {
        super(props)
        this.state = {
            menuItems: {},
            menuCards: [],
            priceOfSelected: 0,
            selectedIds: [],
            showModal: false,
        }
    }

    onClickOrder = (item) => {
        if (item.selectedAmount === 0) {
            this.props.selectItem(item.id, true, this.props.menuItems, this.props.totalPrice)
        }
        this.setState({ showModal: true })
    }

    onClickSelect = (item, add) => {
        this.props.selectItem(item.id, add, this.props.menuItems, this.props.totalPrice)
    }

    makeMenuCards = (menuItems) => {
        return menuItems.map(item => {
            item.euroPrice = parseFloat(this.props.euroRate * item.price).toFixed(2)
            return item
        }).map(item => {
            let price = this.props.currencyMode === '$' ? item.price : item.euroPrice
            return <div key={item.id}>
                <Card style={{ width: '18rem', margin: '10px', height: '530px', background: '#EDF0D5' }}>
                    <Card.Img variant="top" src={item.image_url} />
                    <Card.Body style={{ height: "220px" }}>
                        <Card.Title>{item.name}</Card.Title>
                        <Card.Text>{item.description}</Card.Text>
                    </Card.Body>
                    <ListGroup style={{ background: '#EDF0D5' }} className="list-group-flush">
                        <ListGroupItem style={{ background: '#EDF0D5' }}>{price}{this.props.currencyMode}</ListGroupItem>
                    </ListGroup>
                    <Card.Body>
                        <Button onClick={() => { this.onClickOrder(item) }} style={{ background: '#6F7070', margin: '3px', border: 'none' }}>
                            Order now
                        </Button>
                        <Button onClick={() => { this.onClickSelect(item, false) }} variant="outline-secondary" size="sm" style={{margin:'3px'}}>
                            -
                        </Button>
                        {item.selectedAmount}
                        <Button onClick={() => { this.onClickSelect(item, true) }} variant="outline-secondary" size="sm" style={{margin:'3px'}}>
                            +
                        </Button>
                    </Card.Body>
                </Card>
            </div>
        })
    }

    render() {
        if (this.props.menuItems) {
            let mk = this.makeMenuCards(this.props.menuItems)
            return (
                <div>
                    <Container className="home-back">
                        <Row className="justify-content-md-center">
                            {mk}
                        </Row>
                    </Container>
                    <OrderModal show={this.state.showModal} onHide={() => this.setState({ showModal: false })}/>
                </div>
            )
        } else {
            return (
                <div>
                    <Container className="home-back">
                        <Row className="justify-content-md-center">
                        <img src='https://acegif.com/wp-content/uploads/loading-11.gif'></img>
                        </Row>
                    </Container>
                </div>
            )
        }

    }
}

const mapStateToProps = state => ({
    currencyMode: state.orders.currencyMode,
    loading: state.orders.loading,
    euroRate: state.orders.euroRate,
    menuItems: state.orders.menuItems,
    totalPrice: state.orders.totalPrice,
})

const mapActionsToProps = {
    setCurrency: actions.setCurrency,
    getMenu: actions.getMenu,
    selectItem: actions.selectItem,
}

export default connect(mapStateToProps, mapActionsToProps)(Menu);