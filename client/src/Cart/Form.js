import React, { Component } from 'react'
import { Form, Button, Col } from 'react-bootstrap'
import { connect } from 'react-redux'
import { actions } from '../_actions'
import { api } from '../_services/api'

class OrderForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            firstname: '',
            surname: '',
            email: '',
            phone: '',
            address: '',
            alert: '',
            alertColor: 'red',
        }
    }

    onChange = e => {
        this.setState({ [e.target.id]: e.target.value })
    }

    validateEmail = (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return re.test(String(email).toLowerCase());
    }

    validatePhone = (phone) => {
        const re = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/
        return re.test(String(phone).toLowerCase());
    }

    onSubmit = e => {
        e.preventDefault()
        const data = this.state
        if (this.props.totalPrice <= 0) {
            this.setState({ alert: 'Choose something from menu', alertColor: 'red' })
        } else if (data.firstname && data.surname && data.email && data.phone && data.address) {
            if (!this.validateEmail(data.email)) {
                this.setState({ alert: 'Email is not correct', alertColor: 'red' })
                return
            }
            if (!this.validatePhone(data.phone)) {
                this.setState({ alert: 'Phone number is not correct', alertColor: 'red' })
                return
            }
            data.amount = this.props.totalPrice + 5
            api.createOrder(data).then(() => {
                this.setState({ alert: 'Success!', alertColor: 'green' })
                this.props.clearOrderInfo(this.props.menuItems)
            }).catch(e => this.setState({ alert: e }))
        } else {
            this.setState({ alert: 'Fill in all fields', alertColor: 'red' })
        }
    }

    render() {
        if (!this.props.loading) {
            return (
                <div>
                    <br />
                    <b>Personal info</b>
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="firstname">
                                <Form.Label>Name</Form.Label>
                                <Form.Control onChange={this.onChange} size="sm" type="text" placeholder="Enter name" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="surname">
                                <Form.Label>Surname</Form.Label>
                                <Form.Control onChange={this.onChange} size="sm" type="text" placeholder="Enter surname" />
                            </Form.Group>
                        </Form.Row>
                        <Form.Group controlId="address">
                            <Form.Label>Address</Form.Label>
                            <Form.Control onChange={this.onChange} size="sm" type="text" placeholder="Enter address" />
                        </Form.Group>
                        <Form.Row>
                            <Form.Group as={Col} controlId="phone">
                                <Form.Label>Phone</Form.Label>
                                <Form.Control onChange={this.onChange} size="sm" type="phone" placeholder="Enter phone number" />
                            </Form.Group>
                            <Form.Group as={Col} controlId="email">
                                <Form.Label>Email</Form.Label>
                                <Form.Control onChange={this.onChange} size="sm" type="email" placeholder="Enter email" />
                            </Form.Group>
                        </Form.Row>
                        <Button onClick={this.onSubmit} variant="outline-secondary" size="sm" type="submit">
                            Submit
                        </Button>
                    </Form>
                    <br />
                    <b style={{ color: this.state.alertColor }}>{this.state.alert}</b>
                </div>
            )
        }
    }
}

const mapStateToProps = state => ({
    currencyMode: state.orders.currencyMode,
    loading: state.orders.loading,
    euroRate: state.orders.euroRate,
    menuItems: state.orders.menuItems,
    totalPrice: state.orders.totalPrice,
})

const mapActionsToProps = {
    setCurrency: actions.setCurrency,
    setEuroRate: actions.setEuroRate,
    clearOrderInfo: actions.clearOrderInfo,
}

export default connect(mapStateToProps, mapActionsToProps)(OrderForm);