import React, { Component } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { connect } from 'react-redux'
import { actions } from '../_actions'
import Selected from './Selected'
import Form from './Form'

class Cart extends Component {
    render() {
        if (!this.props.loading) {
            return (
                <div>
                    <Container className="home-back">
                        <Row className="justify-content-md-center">
                            <Col>
                                <Selected />
                            </Col>
                            <Col>
                                <Form />
                            </Col>
                        </Row>
                    </Container>
                </div>
            )
        } else {
            return (
                <div>
                    <Container className="home-back">
                        <Row className="justify-content-md-center">
                        <img src='https://acegif.com/wp-content/uploads/loading-11.gif'></img>
                        </Row>
                    </Container>
                </div>
            )
        }
    }
}

const mapStateToProps = state => ({
    currencyMode: state.orders.currencyMode,
    loading: state.orders.loading,
    euroRate: state.orders.euroRate
})

const mapActionsToProps = {
    setCurrency: actions.setCurrency,
    setEuroRate: actions.setEuroRate,
}

export default connect(mapStateToProps, mapActionsToProps)(Cart);