import React, { Component } from 'react'
import { Card, ListGroup, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { actions } from '../_actions'

class Selected extends Component {
    
    onClickSelect = (item, add) => {
        this.props.selectItem(item.id, add, this.props.menuItems, this.props.totalPrice)
    }

    makeSelectedList = () => {
        try {
            const deliveryCost = this.props.currencyMode === '$' ? 5 : parseFloat(5 * this.props.euroRate).toFixed(2)
            let sum = deliveryCost
            const moneySymbol = this.props.currencyMode === '$' ? '$' : '€'
            let items = this.props.menuItems.map(item => {
                if (item.selectedAmount > 0) {
                    let price = this.props.currencyMode === '$' ? item.price : item.euroPrice
                    sum += item.selectedAmount * price
                    return (
                        <ListGroup.Item key={item.id}>
                            {item.name} | Amount: {item.selectedAmount} | 
                            <Button onClick={() => { this.onClickSelect(item, false) }} variant="outline-secondary" size="sm" style={{ margin: '3px' }}>
                                -
                            </Button>
                            {item.selectedAmount}
                            <Button onClick={() => { this.onClickSelect(item, true) }} variant="outline-secondary" size="sm" style={{ margin: '3px' }}>
                                +
                            </Button>
                            | {item.selectedAmount * price} {moneySymbol}
                        </ListGroup.Item>
                    )
                }
            })
            const tail = [
                <ListGroup.Item key='-1'>
                    Delivery: {deliveryCost} {moneySymbol}
                </ListGroup.Item>,
                <ListGroup.Item key='-2'>
                    <b>Total: {sum} {moneySymbol}</b>
                </ListGroup.Item>,
            ]

            return [...items, ...tail]
        } catch (error) {
            return null
        }
    }

    render() {
        if (!this.props.loading) {
            return (
                <div>
                    <br />
                    <b>Your order</b>
                    <Card>
                        <ListGroup variant="flush">
                            {this.makeSelectedList()}
                        </ListGroup>
                    </Card>
                    <br />
                    <Link to="menu" style={{ color: "black" }}>Back to menu</Link>

                </div>
            )
        } else {
            return (<div>Loading...</div>)
        }
    }
}

const mapStateToProps = state => ({
    currencyMode: state.orders.currencyMode,
    loading: state.orders.loading,
    euroRate: state.orders.euroRate,
    menuItems: state.orders.menuItems,
    totalPrice: state.orders.totalPrice,
})

const mapActionsToProps = {
    setCurrency: actions.setCurrency,
    setEuroRate: actions.setEuroRate,
    selectItem: actions.selectItem,
}

export default connect(mapStateToProps, mapActionsToProps)(Selected);