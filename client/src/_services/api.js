import axios from 'axios';

const url = process.env.REACT_APP_API_URL ? process.env.REACT_APP_API_URL : ''

const getMenu = async () => {
    try {
        return axios.get(`${url}get-menu`)
            .then(data => {
                return data
            })
    } catch (error) {
        console.log(error)
    }
}

const getEuroRate = async () => {
    try {
        return axios.get('https://api.ratesapi.io/api/latest?base=USD')
            .then(data => {
                return data.data.rates.EUR
            })
    } catch (error) {
        console.log(error)
    }
}

const createOrder = async (data) => {
    try {
        axios({
            method: 'post',
            url: `${url}create-order`,
            params: {
                firstname: data.firstname,
                surname: data.surname,
                address: data.address,
                email: data.email,
                phone: data.phone,
                amount: data.amount,
            }
        })
    } catch (error) {
        console.log(error)
    }
}

export const api = {
    getMenu,
    getEuroRate,
    createOrder,
}